USE dbb;
-- SHOW TABLES;

-- ADD LEASE CNTRACT
DROP PROCEDURE IF EXISTS `add_lease_contract`;
DELIMITER //
CREATE PROCEDURE `add_lease_contract` (IN `ContractConclusionDate` DATE,
                                       IN `ContractEndDate` DATE,
                                       IN `Sum` INT,
                                       IN `ClientNameSurname` VARCHAR(45),
                                       IN `ClientPassportData` VARCHAR(45))
BEGIN
    INSERT INTO `Договор_аренды` (`Дата заключения договора`, `Дата окончания договора`, `Сумма`, `id_заказчика`) 
	SELECT `ContractConclusionDate`, `ContractEndDate`, `Sum`, `Заказчик`.`id_заказчика`
	FROM `Заказчик`
	WHERE `Заказчик`.`ФИО` = `ClientNameSurname` AND `Заказчик`.`Серия-номер паспорта` = `ClientPassportData`;
END //
DELIMITER ;

-- ADD SINGLE CAR DATA TO LEASE CONTRACT
DROP PROCEDURE IF EXISTS `add_car_to_lease_contract`;
DELIMITER //
CREATE PROCEDURE `add_car_to_lease_contract` (IN `id_договора` INT,
                                              IN `Гос. номер` VARCHAR(45))
BEGIN
	INSERT INTO `Сведения_об_автомобилях_в_договоре_аренды` (`id_договора`, `id_автомобиля`)
	SELECT `id_договора`, `Автомобиль`.`id_автомобиля`
	FROM `Автомобиль`
	WHERE `Автомобиль`.`Гос. номер` = `Гос. номер`;
END //
DELIMITER ;

-- ADD SINGLE DRIVER INFO TO LEASE CONTRACT
DROP PROCEDURE IF EXISTS `add_driver_to_lease_contract`;
DELIMITER //
CREATE PROCEDURE `add_driver_to_lease_contract` (IN `id_договора` INT,
                                                 IN `NameSurname` VARCHAR(45),
                                                 IN `PassportData` VARCHAR(45))
BEGIN
	INSERT INTO `Сведения_о_водителях_в_договоре_аренды` (`id_договора`, `id_водителя`)
	SELECT `id_договора`, `Водитель`.`id_водителя`
	FROM `Водитель`
	WHERE `Водитель`.`ФИО` = `NameSurname` AND `Водитель`.`Паспортные данные` = `PassportData`;
END //
DELIMITER ;

DROP PROCEDURE IF EXISTS `get_contract_id`;
DELIMITER //
CREATE PROCEDURE `get_contract_id` (IN `ContractConclusionDate` DATE,
                                    IN `ContractEndDate` DATE,
                                    IN `Sum` INT,
                                    IN `ClientNameSurname` VARCHAR(45),
                                    IN `ClientPassportData` VARCHAR(45))
BEGIN
    SET @client_id = NULL;
    SELECT `Заказчик`.`id_заказчика` INTO @client_id FROM `Заказчик` 
        WHERE `Заказчик`.`ФИО` = `ClientNameSurname` 
        AND `Заказчик`.`Серия-номер паспорта`= `ClientPassportData`;

    SELECT `Договор_аренды`.`id_договора`
    FROM `Договор_аренды`
    WHERE `Дата заключения договора` = `ContractConclusionDate` AND
    `Дата окончания договора` = `ContractEndDate` AND
    `Сумма` = `Sum` AND `id_заказчика` = @client_id;

END //
DELIMITER ;
