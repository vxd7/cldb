USE dbb;

DROP VIEW IF EXISTS `occupied_drivers`;
CREATE VIEW `occupied_drivers` AS
    SELECT `id_водителя` , `ФИО` , `Паспортные данные`
    FROM `Сведения_о_водителях_в_договоре_аренды`
    NATURAL JOIN
    `Водитель`;

DROP PROCEDURE IF EXISTS `drivers4car`;
DELIMITER //
CREATE PROCEDURE `drivers4car`(IN `Гос. номер` VARCHAR(45))
BEGIN
    SET @id_car = NULL;
    SELECT `Автомобиль`.`id_автомобиля` INTO @id_car FROM `Автомобиль` WHERE `Автомобиль`.`Гос. номер` = `Гос. номер`;

    SET @insurance_license_id = NULL;
    SELECT `Страховое_свидетельство`.`id_свидетельства` INTO @insurance_license_id
        FROM `Страховое_свидетельство`
        WHERE `Страховое_свидетельство`.`id_автомобиля` = @id_car;

    DROP TEMPORARY TABLE IF EXISTS `selected_drivers_ids`;
    CREATE TEMPORARY TABLE `selected_drivers_ids` AS (
        SELECT insurance_license_drivers.`id_водителя` AS `id_водителя`
        FROM `Сведения_о_водителях_в_страховом_свидетельстве` insurance_license_drivers
        WHERE insurance_license_drivers.`id_свидетельства` = @insurance_license_id);

    DROP TEMPORARY TABLE IF EXISTS `result_drivers4car`;
    CREATE TEMPORARY TABLE `result_drivers4car` AS (
        SELECT drivers.`id_водителя` AS `id_водителя`, drivers.`ФИО` AS `ФИО`, drivers.`Паспортные данные` AS `Паспортные данные`
        FROM `Водитель` drivers, selected_drivers_ids
        WHERE drivers.`id_водителя` = `selected_drivers_ids`.`id_водителя`);

    SELECT DISTINCT result_drivers4car.`id_водителя`, result_drivers4car.`ФИО`, result_drivers4car.`Паспортные данные`
    FROM result_drivers4car LEFT JOIN
        (SELECT * FROM occupied_drivers) occup
    ON result_drivers4car.`id_водителя` = occup.`id_водителя`
    WHERE occup.`id_водителя` IS NULL;

END //
DELIMITER ;
