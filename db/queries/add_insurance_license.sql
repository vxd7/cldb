USE dbb;
-- SHOW TABLES;

-- ADD LEASE CNTRACT
DROP PROCEDURE IF EXISTS `add_insurance_license`;
DELIMITER //
CREATE PROCEDURE `add_insurance_license` (IN `Название страховой компании` VARCHAR(45),
                                          IN `Гос. номер автомобиля` VARCHAR(45))
BEGIN
	INSERT INTO `Страховое_свидетельство` (`id_страховой компании`, `id_автомобиля`) 
	SELECT `Страховая_компания`.`id_страховой компании`, `Автомобиль`.`id_автомобиля`
	FROM `Страховая_компания`, `Автомобиль`
	WHERE `Страховая_компания`.`Название` = `Название страховой компании` AND `Автомобиль`.`Гос. номер` = `Гос. номер автомобиля`;
END //
DELIMITER ;

-- ADD ONE DRIVER TO LEASE CONTRACT
DROP PROCEDURE IF EXISTS `add_driver_to_insurance_license`;
DELIMITER //
CREATE PROCEDURE `add_driver_to_insurance_license` (IN `Гос. номер автомобиля` VARCHAR(45),
                                                    IN `DriverNameSurname` VARCHAR(45),
                                                    IN `DriverPassportData` VARCHAR(45))
BEGIN

    INSERT INTO `Сведения_о_водителях_в_страховом_свидетельстве` (`id_водителя`, `id_свидетельства`)
    SELECT `Водитель`.`id_водителя`, `Страховое_свидетельство`.`id_свидетельства`
    FROM `Водитель`, `Страховое_свидетельство`
    WHERE `Страховое_свидетельство`.`id_автомобиля` IN 
        (SELECT `id_автомобиля` 
        FROM `Автомобиль`
        WHERE `Гос. номер` = `Гос. номер автомобиля`)
    AND (`Водитель`.`ФИО` = `DriverNameSurname` AND `Водитель`.`Паспортные данные` = `DriverPassportData`);
END //
DELIMITER ;
