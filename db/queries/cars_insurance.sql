USE dbb;

DROP VIEW IF EXISTS `all_cars`;
CREATE VIEW `all_cars` AS
    SELECT `Марка`, `Модель`, `Тип кузова`, `Класс`, `Цвет`, `Состояние`, `Год выпуска`, `Гос. номер`
    FROM `Автомобиль`;

DROP VIEW IF EXISTS `cars_without_insurance`;
CREATE VIEW`cars_without_insurance` AS
    SELECT DISTINCT cars.`id_автомобиля`, cars.`Марка`, cars.`Модель`, cars.`Тип кузова`, cars.`Класс`, cars.`Цвет`, cars.`Состояние`, cars.`Гос. номер`, cars.`Год выпуска`
    FROM `Автомобиль` cars LEFT JOIN `Страховое_свидетельство` insurance ON cars.`id_автомобиля` = insurance.`id_автомобиля`
    WHERE insurance.`id_автомобиля` IS NULL;

DROP VIEW IF EXISTS `cars_with_insurance`;
CREATE VIEW`cars_with_insurance` AS
    SELECT DISTINCT cars.`Марка`, cars.`Модель`, cars.`Тип кузова`, cars.`Класс`, cars.`Цвет`, cars.`Состояние`, cars.`Гос. номер`, cars.`Год выпуска`
    FROM `Автомобиль` cars LEFT JOIN cars_without_insurance without
    ON cars.`id_автомобиля` = without.`id_автомобиля`
    WHERE without.`id_автомобиля` IS NULL;
