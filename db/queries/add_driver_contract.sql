USE dbb;
-- SHOW TABLES;

-- ADD LEASE CNTRACT
DROP PROCEDURE IF EXISTS `add_driver_contract`;
DELIMITER //
CREATE PROCEDURE `add_driver_contract` (IN `DriverNameSurname` VARCHAR(45),
                                        IN `DriverPassportData` VARCHAR(45),
                                        IN `ContractConclusionDate` DATE,
                                        IN `ContractEndDate` DATE,
                                        IN `Sum` INT,
                                        IN `CarTypes` VARCHAR(45))
BEGIN
	INSERT INTO `Контракт водителя` (`id_водителя`, `Дата заключения контракта`, `Дата окончания контракта`, `Сумма`, `Тип автомобилей`) 
	SELECT `Водитель`.`id_водителя`, `ContractConclusionDate`, `ContractEndDate`, `Sum`, `CarTypes`
	FROM `Водитель`
	WHERE `Водитель`.`ФИО` = `DriverNameSurname` AND `Водитель`.`Паспортные данные` = `DriverPassportData`;
END //
DELIMITER ;
