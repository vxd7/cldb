USE dbb;
-- SHOW TABLES;

DROP PROCEDURE IF EXISTS `add_new_car`;
DELIMITER //
CREATE PROCEDURE `add_new_car`(IN `StateNumber` varchar(45),
                               IN `Owner` varchar(45),
                               IN `Model` varchar(45),
                               IN `Mark` varchar(45),
                               IN `BodyType` varchar(45),
                               IN `BirthDay` DATE,
                               IN `Class` varchar(45),
                               IN `Color` varchar(45),
                               IN `Сondition` varchar(45))
BEGIN
	INSERT INTO `Автомобиль` (`Гос. номер`, `Владелец`, `Модель`, `Марка`, `Тип кузова`, `Год выпуска`, `Класс`, `Цвет`, `Состояние`) 
	VALUES (`StateNumber`, `Owner`, `Model`, `Mark`, `BodyType`, `BirthDay`, `Class`, `Color`, `Сondition`);
END //
DELIMITER ;

-- CALL `add_new_car`("E496NU", "Name Surname", "E95", "BMW", "sedan", "2018", "S-class", "Black", "Pretty");
-- SELECT * FROM `автомобиль`;
