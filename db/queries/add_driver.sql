USE dbb;
-- SHOW TABLES;

DROP PROCEDURE IF EXISTS `add_driver`;
DELIMITER //
CREATE PROCEDURE `add_driver` (IN `NameSurname` VARCHAR(45),
                               IN `PassportData` VARCHAR(45),
                               IN `Driver License` VARCHAR(45),
                               IN `Driving Experience` INT)
BEGIN
	INSERT INTO `Водитель` (`ФИО`, `Паспортные данные`, `Права`, `Водительский стаж`) 
	VALUES (`NameSurname`, `PassportData`, `Driver License`, `Driving Experience`);
END //
DELIMITER ;
