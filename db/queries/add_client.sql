USE dbb;
-- SHOW TABLES;

DROP PROCEDURE IF EXISTS `add_new_client`;
DELIMITER //
CREATE PROCEDURE `add_new_client` (IN `NameSurname` VARCHAR(45),
                                   IN `PassportData` VARCHAR(45),
                                   IN `PhoneNumber` VARCHAR(45),
                                   IN `e-mail` VARCHAR(45),
                                   IN `IndividualEntity` VARCHAR(45))
BEGIN
	INSERT INTO `Заказчик` (`ФИО`, `Серия-номер паспорта`, `Телефон`, `e-mail`, `Физ. лицо/Юр. лицо`) 
	VALUES (`NameSurname`, `PassportData`, `Phonenumber`, `e-mail`, `IndividualEntity`);
END //
DELIMITER ;

-- CALL `add_new_client`("AB", "1234123456", "7926374827", "ab@yandex.ru", "Individual");
-- SELECT * FROM `Заказчик`;
