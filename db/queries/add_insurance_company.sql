USE dbb;
-- SHOW TABLES;

-- ADD LEASE CNTRACT
DROP PROCEDURE IF EXISTS `add_insurance_company`;
DELIMITER //
CREATE PROCEDURE `add_insurance_company` (IN `Название` VARCHAR(45),
                                          IN `Номер договора на оказание услуг страхования` INT)
BEGIN
	INSERT INTO `Страховая_компания` (`Название`, `Номер договора на оказание услуг страхования`) 
	VALUES (`Название`, `Номер договора на оказание услуг страхования`);
END //
DELIMITER ;
