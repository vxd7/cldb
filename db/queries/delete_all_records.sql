USE dbb;
-- Remove everything
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE `Автомобиль`;
TRUNCATE TABLE `Заказчик`;
TRUNCATE TABLE `Водитель`;
TRUNCATE TABLE `Договор_аренды`;
TRUNCATE TABLE `Контракт водителя`;
TRUNCATE TABLE `Сведения_об_автомобилях_в_договоре_аренды`;
TRUNCATE TABLE `Сведения_о_водителях_в_договоре_аренды`;
TRUNCATE TABLE `Сведения_о_водителях_в_страховом_свидетельстве`;
TRUNCATE TABLE `Страховая_компания`;
TRUNCATE TABLE `Страховое_свидетельство`;
SET FOREIGN_KEY_CHECKS = 1;
