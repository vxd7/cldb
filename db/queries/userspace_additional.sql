USE dbb;

DROP PROCEDURE IF EXISTS `cars_with_insurance_company`;
DELIMITER //
CREATE PROCEDURE `cars_with_insurance_company`(IN `InsuranceCompanyName` VARCHAR(45))
BEGIN

    SELECT `Гос. номер`, `Владелец`, `Модель`, `Марка`, `Тип кузова`, `Год выпуска`, `Класс`, `Цвет`, `Состояние` FROM 
    (  SELECT `Название`, `id_страховой компании` FROM `Страховая_компания`
        WHERE `Название` = `InsuranceCompanyName`) insurance_comp
    NATURAL JOIN 
    `Страховое_свидетельство`
    NATURAL JOIN 
    `Автомобиль`;
END//
DELIMITER ;

DROP PROCEDURE IF EXISTS `contracts_with_cartype`;
DELIMITER //
CREATE PROCEDURE `contracts_with_cartype`(IN `cartype` VARCHAR(45))
BEGIN
    SELECT `id_договора`, `Дата заключения договора`, `Дата окончания договора`, `Сумма` FROM
    `Договор_аренды`
    NATURAL JOIN `Сведения_об_автомобилях_в_договоре_аренды`
    NATURAL JOIN `Автомобиль`
    WHERE `Тип кузова` = `cartype`;

END//
DELIMITER ;

DROP PROCEDURE IF EXISTS `drivers_with_driving_exp`;
DELIMITER //
CREATE PROCEDURE `drivers_with_driving_exp`(IN `driving_exp` INT)
BEGIN
    SELECT * FROM `Водитель`
    WHERE `Водительский стаж` >= `driving_exp`;

END//
DELIMITER ;

DROP PROCEDURE IF EXISTS `sum_of_contracts_during_dates`;
DELIMITER //
CREATE PROCEDURE `sum_of_contracts_during_dates`(IN `date_start` DATE, IN `date_end` DATE)
BEGIN
    SELECT SUM(`Договор_аренды`.`Сумма`) AS `SUM`
    FROM `Договор_аренды`
    WHERE `Договор_аренды`.`Дата заключения договора` >= `date_start` AND `Договор_аренды`.`Дата окончания договора` <=  `date_end`;
END//
DELIMITER ;

DROP PROCEDURE IF EXISTS `contracts_during_dates`;
DELIMITER //
CREATE PROCEDURE `contracts_during_dates`(IN `date_start` DATE, IN `date_end` DATE)
BEGIN
    SELECT *
    FROM `Договор_аренды`
    WHERE `Договор_аренды`.`Дата заключения договора` >= `date_start` AND `Договор_аренды`.`Дата окончания договора` <=  `date_end`;
END//
DELIMITER ;
