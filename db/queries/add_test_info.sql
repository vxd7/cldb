USE dbb;
-- Add some cars
CALL `add_new_car`("ABC12345DEF", "Owner1", "Model1", "Mark1", "Kuzov1", "2018-01-13", "class1", "Red", "good");
CALL `add_new_car`("BVF678YHB", "Owner2", "Model2", "Mark2", "Kuzov2", "2018-05-19", "class2", "Blue", "so-so");
CALL `add_new_car`("QWE765NBH", "Owner3", "Model3", "Mark3", "Kuzov3", "2018-12-26", "class3", "Green", "unrepairable");
CALL `add_new_car`("vbwino", "Owner4", "Model4", "Mark4", "Kuzov4", "2018-12-26", "class4", "Green", "unrepairable");
CALL `add_new_car`("b294vu", "Owner5", "Model5", "Mark5", "Kuzov5", "2018-12-26", "class5", "Green", "unrepairable");
CALL `add_new_car`("32fvqecqw", "Owner6", "Model6", "Mark6", "Kuzov6", "2018-12-26", "class6", "Green", "unrepairable");
CALL `add_new_car`("neeu1939238hc", "Owner7", "Model7", "Mark7", "Kuzov7", "2018-12-26", "class7", "Green", "unrepairable");
CALL `add_new_car`("av29v3uvn", "Owner8", "Model8", "Mark8", "Kuzov8", "2018-12-26", "class8", "Green", "unrepairable");
CALL `add_new_car`("cnwjec293fcu", "Owner9", "Model9", "Mark9", "Kuzov9", "2018-12-26", "class9", "Green", "unrepairable");
CALL `add_new_car`("1241325436", "Owner0", "Model0", "Mark0", "Kuzov0", "2018-12-26", "class0", "Green", "unrepairable");

-- Add some drivers
CALL `add_driver`("Sasha White", "123456", "license1", 12);
CALL `add_driver`("Masha Black", "789123", "license2", 1);
CALL `add_driver`("John Johnson", "345678", "license3", 9);
CALL `add_driver`("William Smith", "876544", "license4", 99);
CALL `add_driver`("God IsDead", "098765", "license5", 87);

-- Add some insurance companies
CALL `add_insurance_company`("Alpha", 1);
CALL `add_insurance_company`("Beta", 2);
CALL `add_insurance_company`("Gamma", 3);
CALL `add_insurance_company`("Delta", 4);
CALL `add_insurance_company`("Etha", 5);

-- Add some insurance licenses and driver to them
CALL `add_insurance_license` ("Alpha", "ABC12345DEF");
CALL `add_driver_to_insurance_license`("ABC12345DEF", "Sasha White", "123456");
CALL `add_driver_to_insurance_license`("ABC12345DEF", "William Smith", "876544");

CALL `add_insurance_license` ("Beta", "BVF678YHB");
CALL `add_driver_to_insurance_license`("BVF678YHB", "Masha Black", "789123");

CALL `add_insurance_license` ("Gamma", "QWE765NBH");
CALL `add_driver_to_insurance_license`("QWE765NBH", "John Johnson", "345678");
CALL `add_driver_to_insurance_license`("QWE765NBH", "God IsDead", "098765");

-- Just add some insurance licenses
CALL `add_insurance_license` ("Alpha", "vbwino");
CALL `add_insurance_license` ("Beta", "b294vu");
CALL `add_insurance_license` ("Gamma", "32fvqecqw");
CALL `add_insurance_license` ("Delta", "neeu1939238hc");
CALL `add_insurance_license` ("Etha", "av29v3uvn");
CALL `add_insurance_license` ("Alpha", "cnwjec293fcu");
CALL `add_insurance_license` ("Beta", "1241325436");

-- Add clients
CALL `add_new_client`("Client1Name Client1Surname", "Client1Passport", "Client1PhoneNumber", "Client1EMAIL", "Individual");
CALL `add_new_client`("Client2Name Client2Surname", "Client2Passport", "Client2PhoneNumber", "Client2EMAIL", "Individual");
CALL `add_new_client`("Client3Name Client3Surname", "Client3Passport", "Client3PhoneNumber", "Client3EMAIL", "Individual");
CALL `add_new_client`("Client4Name Client4Surname", "Client4Passport", "Client4PhoneNumber", "Client4EMAIL", "Individual");
CALL `add_new_client`("Client5Name Client5Surname", "Client5Passport", "Client5PhoneNumber", "Client5EMAIL", "Individual");

-- Add lease contracts, cars and drivers to them
CALL `add_lease_contract`("2018-01-01", "2018-01-20", 11000, "Client1Name Client1Surname", "Client1Passport");
CALL `add_car_to_lease_contract`(1, "ABC12345DEF");
CALL `add_driver_to_lease_contract`(1, "Sasha White", "123456");

CALL `add_lease_contract`("2018-02-01", "2018-02-20", 12000, "Client2Name Client2Surname", "Client2Passport");
CALL `add_car_to_lease_contract`(2, "BVF678YHB");
CALL `add_driver_to_lease_contract`(2, "Masha Black", "789123");

-- CALL `add_lease_contract`("2018-03-01", "2018-03-20", 13000, "Client3Name Client3Surname", "Client3Passport");
-- CALL `add_car_to_lease_contract`(3, "QWE765NBH");
-- CALL `add_driver_to_lease_contract`(3, "God IsDead", "098765");
