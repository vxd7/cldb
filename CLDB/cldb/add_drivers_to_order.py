from PyQt5 import QtCore, QtWidgets, uic
from db_mod import call_saved_procedure, call_saved_view
from pprint import pprint

class AddDriversToOrder(QtWidgets.QWidget):
    def __init__(self, add_drivers_to_order_ui, addNewCustomerAndCarsRef, db, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(add_drivers_to_order_ui, self)

        # Set up references for previous form
        self.addNewCustomerAndCarsRef = addNewCustomerAndCarsRef
        self.addedCarsExternalWidget = addNewCustomerAndCarsRef.AddedCarsTableWidget
        self.addedCarsInternalWidget = self.ChosenCarsToFindDriversTableWidget

        addNewCustomerAndCarsRef.addedCarsTableWidget_itemAdded.connect(
            self.addedCarsExternalWidget_itemAdded)
        addNewCustomerAndCarsRef.addedCarsTableWidget_itemRemoved.connect(
            self.addedCarsExternalWidget_itemRemoved)

        choosenCarsTable_selectionModel = self.ChosenCarsToFindDriversTableWidget.selectionModel()
        choosenCarsTable_selectionModel.selectionChanged.connect(self.populateDriversChooseList)

        # Database connection
        self.db = db

        self.AddDriverToChosenButton.clicked.connect(self.addDriversToChosenTable)
        self.DeleteDriversFromChosen.clicked.connect(self.addDriversToAllTable)

        self.uploadBtn.clicked.connect(self.upload2db)

    @QtCore.pyqtSlot(list)
    def addedCarsExternalWidget_itemAdded(self, items):
        allCars = self.ChosenCarsToFindDriversTableWidget

        row_cnt = allCars.rowCount()
        allCars.insertRow(row_cnt)
        for item in items:
            allCars.setItem(row_cnt, item.column(), QtWidgets.QTableWidgetItem(item))

    def _areQTableWidgetRowsIdentical(self, row1, row2):
        row1_cols = len(row1)
        row2_cols = len(row2)

        if row1_cols != row2_cols:
            return False
        
        row1 = sorted(row1, key=lambda x: x.column())
        row2 = sorted(row2, key=lambda x: x.column())

        for row1_col, row2_col in zip(row1, row2):
            if row1_col.text() != row2_col.text():
                return False
        return True

    def _getColsInRow(self, row_number, tablewidget):
        col_cnt = tablewidget.columnCount()
        columns = []
        for col in range(col_cnt):
            columns.append(tablewidget.item(row_number, col))
        return columns

    @QtCore.pyqtSlot(list)
    def addedCarsExternalWidget_itemRemoved(self, items):
        allCars = self.ChosenCarsToFindDriversTableWidget
        row_cnt = allCars.rowCount()

        for row in range(row_cnt):
            colsInRow = self._getColsInRow(row, allCars)
            if self._areQTableWidgetRowsIdentical(items, colsInRow):
                allCars.removeRow(row)
                return
    def _addSingleDriverToDriversList(self, tablewidget, driver_dict):
        rows = {'ФИО':0, 'Паспортные данные':1}

        row_cnt = tablewidget.rowCount()
        tablewidget.insertRow(row_cnt)
        for col_name in rows:
            tablewidget.setItem(row_cnt, rows[col_name],
                                QtWidgets.QTableWidgetItem(driver_dict[col_name]))

    def _addSingleDriverToDriversList_QTableWidgetItem(self, tablewidget, QTableWidgetItemsList):
        row_cnt = tablewidget.rowCount()
        tablewidget.insertRow(row_cnt)
        for item in QTableWidgetItemsList:
            tablewidget.setItem(row_cnt, item.column(), QtWidgets.QTableWidgetItem(item))

    def populateDriversChooseList(self):
        allCars = self.ChosenCarsToFindDriversTableWidget
        self.AllDriversTableWidget.setRowCount(0)

        selected_driver = allCars.selectedItems()
        car_state_number = selected_driver[-1].text()

        drivers = call_saved_procedure(self.db, 'drivers4car', [car_state_number])
        for driver in drivers:
            self._addSingleDriverToDriversList(self.AllDriversTableWidget, driver)

    def addDriversToChosenTable(self):
        selected_driver = self.AllDriversTableWidget.selectedItems()
        if not selected_driver:
            return
        self._addSingleDriverToDriversList_QTableWidgetItem(self.ChosenDriversTableWidget, selected_driver)
        row_number = self.AllDriversTableWidget.row(selected_driver[0])
        self.AllDriversTableWidget.removeRow(row_number)

    def addDriversToAllTable(self):
        selected_driver = self.ChosenDriversTableWidget.selectedItems()
        if not selected_driver:
            return
        self._addSingleDriverToDriversList_QTableWidgetItem(self.AllDriversTableWidget, selected_driver)
        row_number = self.ChosenDriversTableWidget.row(selected_driver[0])
        self.ChosenDriversTableWidget.removeRow(row_number)

    def get_contract_id(self, contract_data):
        res_rows = call_saved_procedure(self.db, 'get_contract_id', contract_data)[0]
        return res_rows['id_договора']
        
    def uploadCar2db(self, contract_id, car_cols):
        if type(car_cols) not in [list, tuple]:
            print("Invalid type in uploadCar2db func!\nShould be list")
            return
        proc_args = [contract_id]
        proc_args.extend(car_cols)
        print(proc_args)
        call_saved_procedure(self.db, 'add_car_to_lease_contract', proc_args)

    def uploadDriver2db(self, contract_id, driver_cols):
        if type(driver_cols) not in [list, tuple]:
            print("Invalid type in uploadDriver2db func!\nShould be list")
            return
        proc_args = [contract_id]
        proc_args.extend(driver_cols)
        print(proc_args)
        call_saved_procedure(self.db, 'add_driver_to_lease_contract', proc_args)

    def upload2db(self):
        contractStartDate = QtCore.QDate.currentDate().toString(QtCore.Qt.ISODate)
        contractFio, contractEndDate, contractSum = self.addNewCustomerAndCarsRef.getTextData()

        proc_args = [str(contractStartDate), contractEndDate, contractSum, contractFio, 'Client1Passport']
        call_saved_procedure(self.db, 'add_lease_contract', proc_args)

        contract_id = self.get_contract_id(proc_args)
        # Get car list
        allCars = self.ChosenCarsToFindDriversTableWidget
        for row in range(allCars.rowCount()):
            nummm = allCars.item(row, 6).text()
            print(nummm)
            self.uploadCar2db(contract_id, [nummm])

        # Get driver list
        chosenDrivers = self.ChosenDriversTableWidget
        for row in range(chosenDrivers.rowCount()):
            driverName = chosenDrivers.item(row, 0).text()
            driverSurname = chosenDrivers.item(row, 1).text()
            print(driverName, driverSurname)
            # upload here
            self.uploadDriver2db(contract_id, [driverName, driverSurname])
