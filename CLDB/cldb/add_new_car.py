from PyQt5 import QtCore, QtWidgets, uic
from db_mod import call_saved_procedure, call_saved_view
from pprint import pprint

class AddNewCar(QtWidgets.QWidget):
    def __init__(self, add_new_car_ui, db, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(add_new_car_ui, self)

        self.db = db

        self.populateCarPreviewWidget()
        self.UploadButton.clicked.connect(self.uploadCar2db)

    def getTextData(self):
       self.carStateNumber = self.StateNumberLine.text()
       self.carOwner = self.OwnerLine.text()
       self.carMark = self.MarkLine.text()
       self.carModel = self.ModelLine.text()

       self.carBodyType = self.BodyTypeComboBox.currentText()
       self.carBirthDate = self.BirthDayLine.text()
       self.carClass = self.ClassComboBox.currentText()
       self.carColor = self.ColorLine.text()

       self.carCondition = self.ConditionComboBox.currentText()

       if not(self.carStateNumber and self.carOwner and self.carMark and
              self.carModel and self.carBodyType and self.carBirthDate and
              self.carClass and self.carColor):
           return False
       return True
    
    def _setSingleCarInCarTable(self, carTableWidget, car_dict):
        rows = {'Марка':0, 'Модель':1, 'Тип кузова':2, 'Класс':3, 'Цвет':4,
                'Состояние':5, 'Год выпуска':6, 'Гос. номер':7}
        carPreview = carTableWidget# Link

        row_cnt = carPreview.rowCount()
        carPreview.insertRow(row_cnt)
        for col_name in rows:
            carPreview.setItem(row_cnt, rows[col_name], QtWidgets.QTableWidgetItem(car_dict[col_name]))

    def populateCarPreviewWidget(self):
        self.PreviewNewCarTableWidget.setRowCount(0)
        valid_cars = call_saved_view(self.db, 'all_cars')
        
        for car in valid_cars:
            car['Год выпуска'] = car['Год выпуска'].strftime('%Y-%m-%d')
            self._setSingleCarInCarTable(self.PreviewNewCarTableWidget, car)

    def uploadCar2db(self):
        # Get all the fields
        check = self.getTextData()

        # Check data
        if not check:
            print("Incorrect car data!")
            return
        proc_args = [self.carStateNumber, self.carOwner, self.carModel, self.carMark,
                     self.carBodyType, self.carBirthDate, self.carClass,
                     self.carColor, self.carCondition]
        call_saved_procedure(self.db, 'add_new_car', proc_args)
        self.populateCarPreviewWidget()

