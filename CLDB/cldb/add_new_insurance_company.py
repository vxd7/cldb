from PyQt5 import QtCore, QtWidgets, uic
from db_mod import call_saved_procedure, call_saved_view
from pprint import pprint

class AddNewInsuranceCompany(QtWidgets.QWidget):
    def __init__(self, add_new_insurance_company_ui, db, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(add_new_insurance_company_ui, self)

        self.db = db

        self.UploadButton.clicked.connect(self.upload2db)

    def getTextData(self):
        self.insuranceCompName = self.InsuranceCompanyNameLine.text()
        self.insuranceDocNum = self.InsuranceDocumentNumberLine.text()

        if not(self.insuranceCompName and self.insuranceDocNum):
            return False
        return True

    def upload2db(self):
        check = self.getTextData()

        if not check:
            QtWidgets.QMessageBox.warning(self, "Insurance Company Addition",
                                          "Incorrect data entered for insurance company")
            return

        proc_args = [self.insuranceCompName, self.insuranceDocNum]
        call_saved_procedure(self.db, 'add_insurance_company', proc_args)
        QtWidgets.QMessageBox.information(self, "Insurance Company Addition", "Successfully added!")
