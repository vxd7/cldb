from PyQt5 import QtCore, QtWidgets, uic
from db_mod import call_saved_procedure, call_saved_view, exec_query
from pprint import pprint

class AddNewInsuranceCertificate(QtWidgets.QWidget):
    def __init__(self, add_new_insurance_certificate_ui, db, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(add_new_insurance_certificate_ui, self)

        self.db = db

        previewCarTable_selectionModel = self.PreviewCarTableWidget.selectionModel()
        previewCarTable_selectionModel.selectionChanged.connect(self.populateLeftDriverList)

        self.AddOneDriverToAddedButton.clicked.connect(self.moveSelectedDriverToRightList)
        self.DeleteFromAddedDriversButton.clicked.connect(self.moveSelectedDriverToLeftList)

        self.UploadButton.clicked.connect(self.upload2db)

        self.populateInsuranceCompNameComboBox()
        self.populateCarList()

    def populateInsuranceCompNameComboBox(self):
        insuranceCompNames = exec_query(self.db, """SELECT `Название`
                                                    FROM `Страховая_компания`;""")
        ins_names = [ins_name['Название'] for ins_name in insuranceCompNames]
        for ins_name in ins_names:
            self.InsuranceNameComboBox.addItem(ins_name)

    def _setSingleCarInCarList(self, carTableWidget, car_dict):
        rows = {'Гос. номер':0, 'Марка':1, 'Модель':2, 'Тип кузова':3, 'Класс':4, 'Цвет':5,
                'Состояние':6, 'Год выпуска':7}
        allCars = carTableWidget # Link

        row_cnt = allCars.rowCount()
        allCars.insertRow(row_cnt)
        for col_name in rows:
            allCars.setItem(row_cnt, rows[col_name], QtWidgets.QTableWidgetItem(car_dict[col_name]))

    def populateCarList(self):
        valid_cars = call_saved_view(self.db, 'cars_without_insurance')
        
        for car in valid_cars:
            car['Год выпуска'] = car['Год выпуска'].strftime('%Y-%m-%d')
            self._setSingleCarInCarList(self.PreviewCarTableWidget, car)

    def getCarTypes4driver(self, id_driver):
        query_res = exec_query(self.db, """SELECT `Тип автомобилей`
                                           FROM `Контракт водителя`
                                           WHERE `id_водителя` = {0}""".format(id_driver))
        car_types = []
        if len(query_res):
            car_types = query_res[0]['Тип автомобилей'].split(',')
            car_types = [car_type.strip() for car_type in car_types]
        return car_types

    def _setSingleDriverInDriverList(self, driverTableWidget, car_dict):
        rows = {'ФИО':0, 'Номер паспорта':1, 'Номер водительских прав':2, 'Стаж':3}
        row_conv = {'ФИО':'ФИО', 'Номер паспорта':'Паспортные данные', 
                    'Номер водительских прав':'Права', 'Стаж':'Водительский стаж'}
        allCars = driverTableWidget # Link

        row_cnt = allCars.rowCount()
        allCars.insertRow(row_cnt)
        for col_name in rows:
            data = str(car_dict[row_conv[col_name]])
            allCars.setItem(row_cnt, rows[col_name], QtWidgets.QTableWidgetItem(data))

    def populateLeftDriverList(self):
        self.AllDriversForThisCarTableWidget.setRowCount(0)
        selectedCar = self.PreviewCarTableWidget.selectedItems()
        if not len(selectedCar):
            return

        selectedCarType = selectedCar[3].text() # Тип кузова

        allDrivers = exec_query(self.db, """SELECT `id_водителя`, `ФИО`, `Паспортные данные`, `Права`, `Водительский стаж`
                                            FROM `Водитель`;""")
        suitableDrivers = []
        for driver in allDrivers:
            if selectedCarType in self.getCarTypes4driver(driver['id_водителя']):
                suitableDrivers.append(driver)
        for driver in suitableDrivers:
            self._setSingleDriverInDriverList(self.AllDriversForThisCarTableWidget, driver)


    def _setSingleDriverInDriverList_QTableWdgetItem(self, driverTableWidget, QTableWidgetItemsList):
        allDrivers = driverTableWidget # Link

        row_cnt = allDrivers.rowCount()
        allDrivers.insertRow(row_cnt)
        for item in QTableWidgetItemsList:
            allDrivers.setItem(row_cnt, item.column(), QtWidgets.QTableWidgetItem(item))

    def moveSelectedDriverToRightList(self):
        selected_driver = self.AllDriversForThisCarTableWidget.selectedItems()
        if not selected_driver:
            return

        self._setSingleDriverInDriverList_QTableWdgetItem(self.AddedDriversTableWidget, selected_driver)
        row_number = self.AllDriversForThisCarTableWidget.row(selected_driver[0])
        self.AllDriversForThisCarTableWidget.removeRow(row_number)

    def moveSelectedDriverToLeftList(self):
        selected_driver = self.AddedDriversTableWidget.selectedItems()
        if not selected_driver:
            return

        self._setSingleDriverInDriverList_QTableWdgetItem(self.AllDriversForThisCarTableWidget, selected_driver)
        row_number = self.AddedDriversTableWidget.row(selected_driver[0])
        self.AddedDriversTableWidget.removeRow(row_number)

    def upload2db(self):
        selectedCar = self.PreviewCarTableWidget.selectedItems()
        selected_drivers_table = self.AddedDriversTableWidget

        # Add insurance license
        stateNumber = selectedCar[0].text()
        insuranceName = self.InsuranceNameComboBox.currentText()

        proc_args = [insuranceName, stateNumber]
        call_saved_procedure(self.db, 'add_insurance_license', proc_args)

        for row in range(selected_drivers_table.rowCount()):
            driverName = selected_drivers_table.item(row, 0).text()
            driverPassport = selected_drivers_table.item(row, 1).text()
            proc_args = [stateNumber, driverName, driverPassport]
            pprint(proc_args)
            call_saved_procedure(self.db, 'add_driver_to_insurance_license', proc_args)
