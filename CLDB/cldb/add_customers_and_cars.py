from PyQt5 import QtCore, QtWidgets, uic
from db_mod import call_saved_view

class AddCustomerAndCars(QtWidgets.QWidget):
    addedCarsTableWidget_itemAdded = QtCore.pyqtSignal(list)
    addedCarsTableWidget_itemRemoved = QtCore.pyqtSignal(list)

    def __init__(self, add_customer_and_cars_ui, db_connection, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(add_customer_and_cars_ui, self)

        self.db = db_connection
        self.populateLeftCarList()

        self.AllCarsChosenSendToAddedButton.clicked.connect(self.moveSelectedCarToRightList)
        self.DeleteCarFromAddedButton.clicked.connect(self.moveSelectedCarToLeftList)

    def getTextData(self):
        m_fio = self.CostumerNameLine.text()
        m_date = self.DateLine.text()
        m_sum = self.SumLine.text()

        return(str(m_fio), str(m_date), int(m_sum))

    def _setSingleCarInCarList(self, carTableWidget, car_dict):
        rows = {'Марка':0, 'Модель':1, 'Тип кузова':2, 'Класс':3, 'Цвет':4,
                'Состояние':5, 'Гос. номер':6}
        allCars = carTableWidget # Link

        row_cnt = allCars.rowCount()
        allCars.insertRow(row_cnt)
        for col_name in rows:
            allCars.setItem(row_cnt, rows[col_name], QtWidgets.QTableWidgetItem(car_dict[col_name]))

    def _setSingleCarInCarList_QTableWdgetItem(self, carTableWidget, QTableWidgetItemsList):
        allCars = carTableWidget # Link

        row_cnt = allCars.rowCount()
        allCars.insertRow(row_cnt)
        for item in QTableWidgetItemsList:
            allCars.setItem(row_cnt, item.column(), QtWidgets.QTableWidgetItem(item))

    def populateLeftCarList(self):
        valid_cars = call_saved_view(self.db, 'cars_with_insurance')
        
        for car in valid_cars:
            self._setSingleCarInCarList(self.AllCarsTableWidget, car)

    def moveSelectedCarToRightList(self):
        selected_car = self.AllCarsTableWidget.selectedItems()
        if not selected_car:
            return
        # Emit addition signal here
        self.addedCarsTableWidget_itemAdded.emit(selected_car)

        self._setSingleCarInCarList_QTableWdgetItem(self.AddedCarsTableWidget, selected_car)
        row_number = self.AllCarsTableWidget.row(selected_car[0])
        self.AllCarsTableWidget.removeRow(row_number)

    def moveSelectedCarToLeftList(self):
        selected_car = self.AddedCarsTableWidget.selectedItems()
        if not selected_car:
            return
        # Emit deletion signal here
        self.addedCarsTableWidget_itemRemoved.emit(selected_car)

        self._setSingleCarInCarList_QTableWdgetItem(self.AllCarsTableWidget, selected_car)
        row_number = self.AddedCarsTableWidget.row(selected_car[0])
        self.AddedCarsTableWidget.removeRow(row_number)
