import os
import sys

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from db_mod import connect, disconnect

from administrative_tabs import AdministrativeTabs

UI_FOLDER = os.path.join(os.path.dirname(__file__), 'GUI')

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, mainwindow_file_ui):
        QtWidgets.QWidget.__init__(self)

        self.db = connect('localhost', 'root', 'qwerty')

        # Load ui file for MainWindow
        uic.loadUi(mainwindow_file_ui, self)

        self.admin_tabs = AdministrativeTabs(
                os.path.join(UI_FOLDER, 'administrative_tabs.ui'), self.db, parent=self.centralwidget)
        self.verticalLayout.addWidget(self.admin_tabs)

    def __del__(self):
        disconnect(self.db)
