import os
from PyQt5 import QtCore, QtWidgets, uic
from add_customers_and_cars import AddCustomerAndCars
from add_drivers_to_order import AddDriversToOrder
from make_order import AddOrder
from add_new_insurance_company import AddNewInsuranceCompany
from add_new_insurance_certificate import AddNewInsuranceCertificate
from add_new_car import AddNewCar

UI_FOLDER = os.path.join(os.path.dirname(__file__), 'GUI')

class AdministrativeTabs(QtWidgets.QWidget):
    def __init__(self, administrative_tabs_ui, db, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(administrative_tabs_ui, self)

        self.db = db

        # Set tabs here
        # Add new customer and cars subtab
        self.add_customers_cars_widget = AddCustomerAndCars(os.path.join(
            UI_FOLDER, 'addNewCostumerAndCars.ui'), self.db, parent=self.addNewCostumerAndCarsWidget)
        self.addNewCostumerAndCarsLayout.addWidget(self.add_customers_cars_widget)

        # Add drivers to order subtab
        self.add_drivers_to_order = AddDriversToOrder(os.path.join(
            UI_FOLDER, 'adddriverstoorder.ui'), self.add_customers_cars_widget, self.db, parent=self.addDriversToOrderWidget)
        self.addDriversToOrderLayout.addWidget(self.add_drivers_to_order)

        # Add new car tab
        self.add_new_car = AddNewCar(os.path.join(
            UI_FOLDER, 'addnewcar.ui'), self.db, parent=self.AddNewCarWidget)
        self.AddNewCarLayout.addWidget(self.add_new_car)

        # Add new insurance company tab
        self.add_new_insurance_comp = AddNewInsuranceCompany(os.path.join(
            UI_FOLDER, 'addnewinsurancecompany.ui'), self.db, parent=self.AddNewCompanyWidget)
        self.AddNewCompanyLayout.addWidget(self.add_new_insurance_comp)

        # Add new insurance certificate and drivers to certificate tab
        self.add_new_insurance_cert = AddNewInsuranceCertificate(os.path.join(
            UI_FOLDER, 'addnewcertificate.ui'), self.db, parent=self.AddNewCertificateWidget)
        self.AddNewCertificateLayout.addWidget(self.add_new_insurance_cert)

