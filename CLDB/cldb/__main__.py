import os
import sys

from PyQt5 import QtCore, QtGui, QtWidgets
from mainwindow import MainWindow

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow(os.path.join(os.path.dirname(__file__), 'GUI', 'mainwindow.ui'))
    main_window.show()
    sys.exit(app.exec_())

