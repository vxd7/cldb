from PyQt5 import QtCore, QtWidgets, uic

class AddOrder(QtWidgets.QWidget):
    def __init__(self, add_order_ui, parent=None):
        QtWidgets.QWidget.__init__(self, parent)
        uic.loadUi(add_order_ui, self)
